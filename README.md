# pcl_registration


# Point cloud registration using Iterative Cloud Points method
The ICP algorithm utilizes point-to-point method for cloud registration. 
pcd files are available under the folder and some images with 2 point clouds and 16 point clouds. 

# Instruction
1. Run catkin_make in catkin workspace
2. Open another terminal and run roscore
3. Open a third terminal and run rosbag play yourbagfile.bag
4. Go to catkin_ws/devel/lib
5. Run ./pcl_1

If you want to use pcl_template file

1. Run cmake ..

2. make

3. go to pcl_registration/lib

4. ./pcl_template


# Point cloud registration using Normal Distributions Transform method
The NDT algorithm is a registration algorithm that uses standard optimization techniques applied to statistical models of 3D points to determine the 
most probable registration between two point clouds.

The rosbag is played in one terminal to publish the pcd messages and ndt.cpp file subcribes to these velodyne messages. 
Two pcd data is subcribed and then downsampled using voxel filter with cube size of 0.3 for x,y and z direction. Filtered pcd data is alingned using ndt transformation pcl ros commands.
The final transformation is developed by imposing the filtered alinged ndt transformed matrix over the initial current cloud (not filtered) to create map
with more points to be densed and able to view more rigid lines than countable points. The loop for 10 to 20 pcd files is been filtered and mapped. 
The final registration is obtained withn 5 mintutes for 10 pcd data using ndt transformation.

pcd files and images are available under the folder named "final_op" under src directory for 20 pcd subsribed data 
and in another folder named "images" under bag directory with 10 pcd subcribed data respectively. 

# Instruction
1. Change the path of the final pcd file to be saved in ndt.cpp file
2. Run catkin_make in catkin workspace
3. Open another terminal and run roscore
4. Open a third terminal and run rosbag play your bagfile.bag(campus_scan.bag in this case) to publish data at required rate
5. Go to catkin_ws/devel/lib
6. Run ./ndt to subcribe pcd data and built the registred map


