// template for PCL mapping
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <iostream>
#include <Eigen/Core>
#include <ros/ros.h>
// PCL includes
#include <sensor_msgs/PointCloud2.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/filters/approximate_voxel_grid.h>
#include <pcl/registration/ndt.h>
#include <pcl/registration/icp.h>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/common/common.h>
#include <pcl/common/centroid.h>
#include <pcl/PCLHeader.h>
#include <visualization_msgs/Marker.h>
#include <pcl_ros/transforms.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/visualization/cloud_viewer.h>

// Subscribers
ros::Subscriber sub;
// Publishers
// ros::Publisher map;

// callback signature

int i = 0;
pcl::PointCloud<pcl::PointXYZ>::Ptr prev_cloud(new pcl::PointCloud<pcl::PointXYZ>);
pcl::PointCloud<pcl::PointXYZ>::Ptr final_map(new pcl::PointCloud<pcl::PointXYZ>);
  
void callback(const sensor_msgs::PointCloud2ConstPtr& cloud_in)
{
  // initialize current PointCloud
  pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_1(new pcl::PointCloud<pcl::PointXYZ>);
  pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_2(new pcl::PointCloud<pcl::PointXYZ>);

  // populate PointCloud with points
  pcl::fromROSMsg(*cloud_in, *cloud_1);
  
  if(i == 0) 
  {
   *prev_cloud = *cloud_1;
   std::cout<<"First cloud saved \n"<<"Size of cloud: "<<prev_cloud->size()<<std::endl;
   i++;
   return;
  }
  
  //remove NAN points from the cloud
  // indices to remove NaN
  {
    std::vector<int> indices;
    pcl::removeNaNFromPointCloud(*cloud_1, *cloud_1, indices);
  }

  // ICP/NDT function here
  // -> if doing NDT, remember to perform voxel grid filtering
  pcl::IterativeClosestPoint<pcl::PointXYZ, pcl::PointXYZ> icp;
  
  icp.setTransformationEpsilon (1e-6);
  icp.setMaximumIterations (200);
//  icp.setTransformationEpsilon (1e-9);
//  icp.setMaxCorrespondenceDistance (0.05);
//  icp.setEuclideanFitnessEpsilon (1);
//  icp.setRANSACOutlierRejectionThreshold (1.5);

  std::cout<<"Size of target: "<<prev_cloud->size()<<std::endl;
  std::cout<<"Size of source: "<<cloud_1->size()<<std::endl;
  
  icp.setInputTarget(prev_cloud);
  icp.setInputSource(cloud_1);
  
  
  std::cout<<"Aligning cloud # "<<i<<std::endl;
  pcl::PointCloud<pcl::PointXYZ>::Ptr Final(new pcl::PointCloud<pcl::PointXYZ>);
  
  icp.align(*Final);

  pcl::transformPointCloud(*cloud_1, *Final, icp.getFinalTransformation());

  // save/display point cloud map
  // -> to save map use `savePCDFileASCII` function (see NDT example)
//  pcl::io::savePCDFileASCII ("/home/richard/first_scan.pcd", *Final);
  
  *prev_cloud = *cloud_1;
  *final_map = *final_map + *Final;
  pcl::io::savePCDFileASCII ("/home/richard/final_map.pcd", *final_map);
  
  i++;
  
//  return;
  
  
  // -> to display live map in rviz use publisher for sensor_msgs::PointCloud2
//  sensor_msgs::PointCloud2 current_cloud;
//  pcl::toROSMsg(*cloud_1, *Final);
//  map.publish(Final); 
  
}


int main (int argc, char** argv)
{
  // Initialize ROS
  ros::init(argc, argv, "td_mapping");
  ros::NodeHandle nh;

  // Create a ROS subscriber for the input point cloud
  sub = nh.subscribe("velodyne_points", 1, callback);

  // Spin
  ros::spin();

  return 0;
}
