#include <iostream>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/registration/icp.h>
#include <Eigen/Core>
#include <ros/ros.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <sensor_msgs/PointCloud2.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/filters/approximate_voxel_grid.h>
#include <pcl/registration/ndt.h>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/common/common.h>
#include <pcl/common/centroid.h>
#include <pcl/PCLHeader.h>
#include <visualization_msgs/Marker.h>
#include <pcl_ros/transforms.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/visualization/cloud_viewer.h>



// Subscribers
ros::Subscriber sub;
// Publishers
ros::Publisher map;

// callback signature
void callback(const sensor_msgs::PointCloud2ConstPtr& cloud_in)
{
  //pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_out (new pcl::PointCloud<pcl::PointXYZ>);
  pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_out (new pcl::PointCloud<pcl::PointXYZ>);
 // initialize current PointCloud
  pcl::PointCloud<pcl::PointXYZ>::Ptr current_cloud(new pcl::PointCloud<pcl::PointXYZ>);
  // populate PointCloud with points
  pcl::fromROSMsg(*cloud_in, *current_cloud);
  ROS_INFO("done processing message");
 //remove NAN points from the cloud
  // indices to remove NaN
  {
    std::vector<int> indices;
    pcl::removeNaNFromPointCloud(*current_cloud, *current_cloud, indices);
  }
  // Fill in the CloudIn data
  current_cloud->width    = 5;
  current_cloud->height   = 1;
  current_cloud->is_dense = false;
  current_cloud->points.resize (current_cloud->width * current_cloud->height);
  for (size_t i = 0; i < current_cloud->points.size (); ++i)
  {
    current_cloud->points[i].x = 1024 * rand () / (RAND_MAX + 1.0f);
    current_cloud->points[i].y = 1024 * rand () / (RAND_MAX + 1.0f);
    current_cloud->points[i].z = 1024 * rand () / (RAND_MAX + 1.0f);
  }
  std::cout << "Saved " << current_cloud->points.size () << " data points to input:"
      << std::endl;
  for (size_t i = 0; i < current_cloud->points.size (); ++i) std::cout << "    " <<
      current_cloud->points[i].x << " " << current_cloud->points[i].y << " " <<
      current_cloud->points[i].z << std::endl;
  *cloud_out = *current_cloud;
  std::cout << "size:" << cloud_out->points.size() << std::endl;
  for (size_t i = 0; i < current_cloud->points.size (); ++i)
    cloud_out->points[i].x = current_cloud->points[i].x + 0.7f;
  std::cout << "Transformed " << current_cloud->points.size () << " data points:"
      << std::endl;
  for (size_t i = 0; i < cloud_out->points.size (); ++i)
    std::cout << "    " << cloud_out->points[i].x << " " <<
      cloud_out->points[i].y << " " << cloud_out->points[i].z << std::endl;
      
  pcl::IterativeClosestPoint<pcl::PointXYZ, pcl::PointXYZ> icp;
  icp.setInputSource(current_cloud);
  icp.setInputTarget(cloud_out);
  pcl::PointCloud<pcl::PointXYZ> Final;
  icp.align(Final);
  std::cout << "has converged:" << icp.hasConverged() << " score: " <<
  icp.getFitnessScore() << std::endl;
  std::cout << icp.getFinalTransformation() << std::endl;
  //Eigen::Matrix4f transformation = icp.getFinalTransformation ();
  pcl::PointCloud<pcl::PointXYZ>::Ptr icp_cloud (new pcl::PointCloud<pcl::PointXYZ>);
  *icp_cloud=*icp_cloud + Final;
// Saving transformed input cloud.
  pcl::io::savePCDFileASCII ("/home/kasa/Desktop/ICP.pcd", *icp_cloud);
}


int main (int argc, char** argv)
{
  // Initialize ROS
  ros::init(argc, argv, "slam");
  ros::NodeHandle nh;

  // Create a ROS subscriber for the input point cloud
  sub = nh.subscribe("/velodyne_points", 1, callback);

  // Spin
  ros::spin();

  return 0;
}

