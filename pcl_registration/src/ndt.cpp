// template for PCL mapping
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <iostream>
#include <Eigen/Core>
#include <ros/ros.h>
// PCL includes
#include <sensor_msgs/PointCloud2.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/filters/approximate_voxel_grid.h>
#include <pcl/registration/ndt.h>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/common/common.h>
#include <pcl/common/centroid.h>
#include <pcl/PCLHeader.h>
#include <visualization_msgs/Marker.h>
#include <pcl_ros/transforms.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/visualization/cloud_viewer.h>

// Subscribers
ros::Subscriber sub;
// Publishers
//ros::Publisher map;


// callback signature
void callback(const sensor_msgs::PointCloud2ConstPtr& cloud_in)
{

 int count = 0;
 
 pcl::PointCloud<pcl::PointXYZ>::Ptr main_cloud (new pcl::PointCloud<pcl::PointXYZ>);
      
 for(count=0;count<20;count++)
 { 
   // initialize current PointCloud
   pcl::PointCloud<pcl::PointXYZ>::Ptr current_cloud(new pcl::PointCloud<pcl::PointXYZ>);
   // populate PointCloud with points
   pcl::fromROSMsg(*cloud_in, *current_cloud);


   //remove NAN points from the cloud
   // indices to remove NaN
   {
    std::vector<int> indices;
    pcl::removeNaNFromPointCloud(*current_cloud, *current_cloud, indices);
   }



   // Filtering input scan to roughly 10% of original size to increase speed of registration.
   pcl::PointCloud<pcl::PointXYZ>::Ptr filtered_cloud(new pcl::PointCloud<pcl::PointXYZ>);
   pcl::ApproximateVoxelGrid<pcl::PointXYZ>approximate_voxel_filter;
   approximate_voxel_filter.setLeafSize(0.3, 0.3, 0.3);
   approximate_voxel_filter.setInputCloud(current_cloud);
   approximate_voxel_filter.filter(*filtered_cloud);
   std::cout<< "Input cloud contains "<< current_cloud->size()<<" Filtered cloud contains "<< filtered_cloud->size()<< std::endl;

  
   if(count==0)
   {
    *main_cloud = *filtered_cloud;
   } 
   else
   {

    // Initializing Normal Distributions Transform (NDT).
    pcl::NormalDistributionsTransform<pcl::PointXYZ,pcl::PointXYZ> ndt;
 
    // Setting minimum transformation difference for termination condition.
    ndt.setTransformationEpsilon(0.01);
 
    // Setting maximum step size for More-Thuente line search.
    ndt.setStepSize(0.1);
  
    //Setting Resolution of NDT grid structure (VoxelGridCovariance).
    ndt.setResolution(1.0);
  
  
    // Setting max number of registration iterations.
    ndt.setMaximumIterations (35);

    // Setting point cloud to be aligned.
    ndt.setInputSource (filtered_cloud);
 
    // Setting point cloud to be aligned to.
    ndt.setInputTarget (main_cloud);
 
    // Set initial alignment estimate 
   // Eigen::AngleAxisf init_rotation (0.6931, Eigen::Vector3f::UnitZ ());
    //Eigen::Translation3f init_translation (1.79387, 0.720047, 0);
    Eigen::Matrix4f init_guess = ndt.getFinalTransformation ();
    //(init_translation * init_rotation).matrix ();
  
    // Calculating required rigid transform to align the input cloud to the target cloud.
    pcl::PointCloud<pcl::PointXYZ>::Ptr output_cloud (new pcl::PointCloud<pcl::PointXYZ>);
    ndt.align (*output_cloud, init_guess);
  
    std::cout << "Normal Distributions Transform has converged:" << ndt.hasConverged ()
            << " score: " << ndt.getFitnessScore () << std::endl;
  
    pcl::transformPointCloud (*current_cloud, *output_cloud, ndt.getFinalTransformation ());
    
    *main_cloud = *main_cloud + *output_cloud;
    }
    
     
    std::cout<<"count is "<<count<<std::endl;
   } 
   //Saving transformed input cloud.
    cout<<"size of map is: " << main_cloud->size() << endl;
    pcl::io::savePCDFileASCII ("/home/neha3090/catkin_ws_init/src/pcl_registration/pcl_registration/src/op9.pcd",*main_cloud);
    cout<<"Saved"<<std::endl;
    ros::shutdown();  
}


int main(int argc, char** argv)
{
  
  // Initialize ROS
  ros::init(argc, argv, "slam");
  ros::NodeHandle nh;

  // Create a ROS subscriber for the input point cloud

  sub = nh.subscribe("/velodyne_points",1,callback);

  // Spin
  ros::spin();

  return 0;
}
